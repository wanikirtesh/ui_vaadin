export interface StringString {
    [key: string]: string
}

export interface StringObject {
    [key: string]: string|{[key: string]: string}
}