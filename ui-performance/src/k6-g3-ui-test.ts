import http from "k6/http";
import { Options, Scenario } from "k6/options";
import * as G3Pages from "./g3-pages.js"


export const options: Options = {
	scenarios: getRequestedScenario(""),


}

export default function(){
	//http.get("https://google.com");
}

function getRequestedScenario(scenarios: string):{[name:string]:Scenario}{
	return {"Hello":{executor:'per-vu-iterations',
						vus:1,
						iterations:1,
						maxDuration:'30m',
						exec:'atGlance'}
	}
}

export function atGlance(){
	G3Pages.login()
	G3Pages.atGlance()

}