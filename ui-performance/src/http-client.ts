"use strict";
import http, { ObjectBatchRequest, BatchRequest, RefinedResponse, RequestBody } from "k6/http";
import {check, fail} from 'k6';
import {StringString} from "./LooseObject.js";
import * as config from "./config.js"
function checkIfRequestSucceeded(response: RefinedResponse<'none'>): void {
    check(response, {'request succeeded': r => r.status < 400})
    if (response.status >= 400) fail(`${response.request.method} - ${response.url} request failed with status ${response.status}.\n${response.body}`)
}


export function get(path:string,baseURL:string,headers:StringString,execTime=0,tag = {}):HttpTimeResponse{
	const res = http.get(
		`${baseURL}${path}`,
		{
			headers:headers,
			tags:tag,
			timeout:300000
		}
	);
	checkIfRequestSucceeded(res)
	execTime += res.timings.duration;
	logResponse(res)
	return {response:res,execTime:execTime};
}

export function post(path:string,baseURL:string,body:RequestBody|null|string|undefined,headers:StringString,execTime=0,tag = {}):HttpTimeResponse{
	const res = http.post(
		`${baseURL}${path}`,
		body,
		{
			headers:headers,
			tags:tag
		}
	);
	checkIfRequestSucceeded(res)
	execTime += res.timings.duration;
	logResponse(res)
	return {response:res,execTime:execTime};
}



export function put(path:string,baseURL:string,body:RequestBody|null|string|undefined,headers:StringString,execTime=0,tag = {}):HttpTimeResponse{
	const res = http.put(
		`${baseURL}${path}`,
		body,
		{
			headers:headers,
			tags:tag
		}
	);
	checkIfRequestSucceeded(res)
	execTime += res.timings.duration;
	logResponse(res)
	return {response:res,execTime:execTime};
}

export function options(path:string,baseURL:string,body:RequestBody|null|string|undefined,headers:StringString,execTime=0,tag= {}):HttpTimeResponse{
	const res = http.options(
		`${baseURL}${path}`,
		body,
		{
			headers:headers,
			tags:tag
		}
	);
	checkIfRequestSucceeded(res)
	execTime += res.timings.duration;
	logResponse(res)
	return {response:res,execTime:execTime};
}

export function del(path:string,baseURL:string,body:RequestBody|null|string|undefined,headers:StringString,execTime=0,tag = {}):HttpTimeResponse{
	const res = http.del(
		`${baseURL}${path}`,
		body,
		{
			headers:headers,
			tags:tag
		}
	);
	checkIfRequestSucceeded(res)
	execTime += res.timings.duration;
	logResponse(res)
	return {response:res,execTime:execTime};
}


export function retryGet(path:string,baseURL:string,headers:StringString,execTime=0,tag = {},retryCount:number):HttpTimeResponse{
	const dt = new Date();
	const res = http.get(
		`${baseURL}${path}`,
		{
			headers:headers,
			tags:tag,
			timeout:300000
		}
	);
	const dt2 = new Date();
	if(res.error_code==1504 && retryCount>0){
		console.warn(`### Failed ${baseURL}${path} remaining retries ${--retryCount}`)
		execTime += (dt2.getTime()-dt.getTime());
		return retryGet(path,baseURL,headers,execTime,tag,retryCount);
	} else{
		execTime+=res.timings.duration;
		logResponse(res)
		return {response:res,execTime:execTime};
	}
}

export interface HttpTimeResponse {
	response: RefinedResponse<"binary" | "none" | "text">
	execTime: number;
}


export function buildRequest(rqType:string,path:string,baseURL:string,body:RequestBody|null|string|undefined,headers:StringString,tag = {}):ObjectBatchRequest{
	const rq:ObjectBatchRequest= {
		method: rqType.toUpperCase(),
		url: `${baseURL}${path}`,
		body: body,
		params: {
			headers: headers,
			tags:tag
		},
	}
	return rq;
}

export function executeBatch(requests: BatchRequest[],executionTime=0):HttpTimeResponse{
	const responses = http.batch(requests)
	for (let i = 0; i < responses.length; i++) {
		checkIfRequestSucceeded(responses[i])
		executionTime += responses[i].timings.duration;
		logResponse(responses[i])
	}
	return {response:responses[responses.length],execTime:executionTime}
}

function logResponse(res:RefinedResponse<"binary" | "none" | "text">):void{
	if(config.logResponse.toLowerCase()=='true'){
		console.log(`=================${res.request.url}====================`)
		console.log(res.body?.toString())
		console.log("================================================")
	}
}



