export const appUrl = required("g3.app.url")
export const username = required("g3.app.user")
export const password = required("g3.app.password")
export const logResponse = required("logResponse")



function required(name:string):string{
	const value = __ENV[name];
	if(!value) throw new Error(`Please provide env variable ${name}`)
	return value;
}