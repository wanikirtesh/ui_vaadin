@echo off
cd ui-performance && call npm run build && cd ..

for /f "delims== tokens=1,2" %%G in (.env) do set %%G=%%H

k6 run ./ui-performance/dist/k6-g3-ui-test.js